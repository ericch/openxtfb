export KDIR ?= /lib/modules/`uname -r`/build

IVC_INCLUDE_DIR ?= $(KDIR)/include
DDH_INCLUDE_DIR ?= $(KDIR)/include

# Compiler Flags
ccflags-y := -Iinclude/drm -Wall -Werror -I$(IVC_INCLUDE_DIR) -I$(DDH_INCLUDE_DIR)

# Build the OpenXT framebuffer module.
obj-m += openxtfb.o

all: modules

modules:
	$(MAKE) -C $(KDIR) M=$(shell pwd)

modules_install: 
	$(MAKE) -C $(KDIR) M=$(shell pwd) modules_install

clean:
	rm openxtfb.o
	rm openxtfb.ko
